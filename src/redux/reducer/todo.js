const initialState = [];

// import {FETCH_SUCCEEDED, FETCH_FAILED} from '../action/todo';

const todos = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_SUCCEEDED': {
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false,
        },
      ];
    }
    case 'FETCH_FAILED': {
      return state.filter((item) => item.text !== action.text);
    }

    default:
      return state; //state does not change
  }
};

export default todos;
