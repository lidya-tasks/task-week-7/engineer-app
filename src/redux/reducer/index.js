import {combineReducers} from 'redux';
import todos from './todo';
import auth from './auth';
import profile from './profile';

export default combineReducers({
  todos,
  auth,
  profile,
});
