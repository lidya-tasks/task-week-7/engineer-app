import {GET_INTEREST} from './interest_types';

export const getInterest = () => {
  return {type: GET_INTEREST};
};

