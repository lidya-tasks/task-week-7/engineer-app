import {GET_CONTENT_SUCCESS, GET_CONTENT_FAILED} from '../action/content_types';

const initialState = [];

const content = (state = initialState, action) => {
  switch (action.type) {
    case GET_CONTENT_SUCCESS: {
      let newContent = action.payload.filter((payloadItem) => {
        return !state.some((stateItem) => payloadItem.id === stateItem.id);
      });
      return [...state, ...newContent];
    }
    case GET_CONTENT_FAILED: {
      return [];
    }
    default:
      return state;
  }
};

export default content;
