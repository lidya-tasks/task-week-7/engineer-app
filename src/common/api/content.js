import axios from 'axios';
import content from '../../redux/reducer/content';
export function apiFetchContent(interest, headers) {
  console.info('test1', content);
  return axios({
    method: 'GET',
    url: 'http:/localhost:3000/content/' + interest,
    headers,
  });
}
