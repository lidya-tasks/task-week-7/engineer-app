// function interest(action) {
//     try {
//         //interest
//         const resInterest= yield apiInterest(action.payload);

//         if (resInterest && resInterest.data) {
//             //save user interest
//             yield saveInterest(resInterest.data.)
//         }
//     }
// }

import {takeLatest, put, all} from 'redux-saga/effects';
import {apiFetchInterest} from '../../common/api/interest';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_INTEREST,
  GET_INTEREST_SUCCESS,
  GET_INTEREST_FAILED,
} from '../action/interest_types';
import {getContent} from '../action/content';

function* getInterest() {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    // FETCH CONTENT
    const resContentt = yield apiFetchInterest(accountId, headers);
    yield put({type: GET_INTEREST_SUCCESS, payload: resContentt.data});
    console.info(resContentt.data);
    yield all(
      resContentt.data.interests.map((item) => {
        return put(getContent(item));
      }),
    );
  } catch (e) {
    // show alert
    ToastAndroid.showWithGravity(
      'Gagal mengambil data interest',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );

    yield put({type: GET_INTEREST_FAILED});
  }
}

function* interestSaga() {
  yield takeLatest(GET_INTEREST, getInterest);
}

export default interestSaga;
