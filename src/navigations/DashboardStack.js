import React from 'react';
import HomeScreen from '../screens/HomeScreen';
import OtherPage from '../screens/OtherPage';
import TodoScreen from '../screens/TodoScreen';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function DashboardStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen name="Other" component={OtherPage} />
      <Stack.Screen
        name="TodoScreen"
        component={TodoScreen}
        options={{title: 'Todo List'}}
      />
    </Stack.Navigator>
  );
}
