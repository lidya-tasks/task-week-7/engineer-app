import React from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';

function ProfileScreen(props) {
  return (
    <View>
      <Text>Halaman kosong</Text>
      <Button
        color="#444"
        onPress={() => props.processLogout()}
        title="LOGOUT"
      />
    </View>
  );
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  processLogout: () => dispatch({type: 'LOGOUT'}),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
