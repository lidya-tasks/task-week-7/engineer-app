import axios from 'axios';

export function apiFetchInterest(id, headers) {
  return axios({
    method: 'GET',
    url: 'http:/localhost:3000/interest/' + id,
    headers,
  });
}
