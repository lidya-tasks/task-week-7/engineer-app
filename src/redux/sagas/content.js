import {takeEvery, put} from 'redux-saga/effects';
import {apiFetchContent} from '../../common/api/content';
import {ToastAndroid} from 'react-native';
import {getHeaders} from '../../common/function/auth';
import {
  GET_CONTENT,
  GET_CONTENT_SUCCESS,
  GET_CONTENT_FAILED,
} from '../action/content_types';

function* getContent(action) {
  try {
    const headers = yield getHeaders();
    // FETCH CONTENT
    const resContent = yield apiFetchContent(action.interest, headers);
    yield put({type: GET_CONTENT_SUCCESS, payload: resContent.data});
  } catch (e) {
    // show alert
    ToastAndroid.showWithGravity(
      'Gagal mengambil data content',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );

    yield put({type: GET_CONTENT_FAILED});
  }
}

function* contentSaga() {
  yield takeEvery(GET_CONTENT, getContent);
}

export default contentSaga;
