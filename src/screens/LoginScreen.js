/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  Text,
  View,
  TextInput,
  ActivityIndicator, // 3/11
  TouchableOpacity, // 3/11
} from 'react-native';
import {connect} from 'react-redux';
import {loginAction} from '../redux/action/auth'; // 3/11

function LoginScreen(props) {
  const [username, setUsername] = useState('demo');
  const [password, setPassword] = useState('demo123');
  const [message, setMessage] = useState(null); // 3/11

  const login = () => {
    if (!username) {
      setMessage('Username wajib diisi!');
    } else if (!password) {
      setMessage('Password wajib diisi!');
    } else {
      props.processLogin({username, password});
    }
  };

  return (
    <View style={{padding: 20}}>
      <Text>Welcome on board!</Text>
      <Text style={{color: 'red'}}>{message}</Text>

      <TextInput
        onChangeText={(text) => setUsername(text)}
        value={username}
        placeholder="Usename"
        style={{
          marginBottom: 10,
          borderRadius: 50,
          paddingLeft: 20,
          backgroundColor: '#ffffff',
        }}
      />

      <TextInput
        onChangeText={(text) => setPassword(text)}
        value={password}
        placeholder="Password"
        style={{
          marginBottom: 10,
          borderRadius: 50,
          paddingLeft: 20,
          backgroundColor: '#ffffff',
        }}
        secureTextEntry={true}
      />

      {/* <Button
        color="#444"
        onPress={() => props.processLogin(username)}
        title="GO TO MAIN"
      /> */}

      {props.isLoading ? (
        <View
          style={{borderRadius: 50, backgroundColor: '#f2f2f2', padding: 15}}>
          <ActivityIndicator size="large" color="#333333" />
        </View>
      ) : (
        <TouchableOpacity
          onPress={() => login()}
          style={{borderRadius: 50, backgroundColor: '#444', padding: 15}}>
          <Text style={{textAlign: 'center', color: '#fff'}}>Login</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  processLogin: (data) => dispatch(loginAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
