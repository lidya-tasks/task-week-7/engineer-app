import axios from 'axios';

export function apiLogin(dataLogin) {
  return axios({
    method: 'POST',
    url: 'http:/localhost:3000/auth/login',
    data: dataLogin,
  });
}
