import {LOGIN, LOGOUT} from './auth_types';

export const loginAction = (payload) => {
  // payload = {
  //   username: 'string'
  //   password: 'string'
  // }
  return {type: LOGIN, payload};
};

export const logoutAction = () => {
  return {type: LOGOUT};
};
